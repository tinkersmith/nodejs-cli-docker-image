FROM node:10.15-alpine

# Copy configuration template files.
ADD script-service /root/script-service
ADD s6 /etc/s6

# For this image we will prepare to use node-sass to compile and build Sass.
RUN apk add --update --no-cache \
  execline \
  ncurses \
  s6 \
  sed \
  bash

WORKDIR /nodejs/apps

ADD entrypoint.sh /entrypoint.sh
ADD run-yarn.sh /usr/local/bin/run-yarn
ENTRYPOINT ["/entrypoint.sh"]
