#!/bin/bash
#
# Script to build and push a new version of this Docker image.
#

# TODO: Add simple build options to allow building of different environments.
TAG="10.15"
REV="2"

function main() {
  docker build \
    --squash \
    -t "portlandwebworks/nodejs-cli:${TAG}-${REV}" \
    ./
}

main "$@"
