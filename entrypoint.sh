#!/bin/sh
# --------------------------------------------

#
# Add the execution paths for running the package.json scripts.
#
if [ -z "$NODE_EXEC_PATHS" ] || [ $NODE_EXEC_PATHS = '' ]; then
  NODE_EXEC_PATHS="/node/apps"
fi

CWD=$(pwd)

#
# Use Yarn workspaces to run a single build at the base of the install path.
# - The preference should be to use Yarn workspaces when possible.
#
if [ $USE_WORKSPACES = 1 ]; then
  if ! [ -f "${NODE_EXEC_PATHS}/package.json" ] || ! [ -f "${NODE_EXEC_PATHS}/package.json" ]; then
    echo -e "\e[31mA lerna.json and package.json file is required at the base of ${NODE_EXEC_PATHS} to use workspaces.\e[0m"
    exit 1;
  fi

  cd "${NODE_EXEC_PATHS}"
  yarn install
fi

# Iterate through the execute paths
for EXEC_PATH in ${NODE_EXEC_PATHS}; do
  if ! [ -d "${EXEC_PATH}" ]; then
    continue
  fi

  # Create a S6 script to run the NPM application.
  for script in `find "${EXEC_PATH}" -not -path '*/node_modules/*' -not -path '*/libraries/*/*' -mindepth 2 -maxdepth 6 -name "package.json" -print`; do

    # Determine the file names and settings.
    scriptDir=$(dirname ${script})
    dirname=$(basename ${scriptDir})

    cd "${scriptDir}"

    if [ -z "${USE_WORKSPACES}" ] || [ $USE_WORKSPACES -ne 1 ]; then
      echo -e "Checking resources and dependencies for \e[32m${dirname}\e[0m!"
      yarn install
    fi

    if [[ "${dirname}" = "libraries" ]]; then
      /usr/local/bin/run-yarn "${scriptDir}"
    else
      s6Dir="/etc/s6/nodejs-${dirname}"

      cp -a /root/script-service/. $s6Dir

      # Replace the running folder directory of this file.
      sed -i -e s:^BASEDIR=.*$:BASEDIR=${scriptDir}: "$s6Dir/run"
      sed -i -e s:^BASEDIR=.*$:BASEDIR=${scriptDir}: "$s6Dir/finish"

      chmod ug+x $s6Dir/run
      chmod ug+x $s6Dir/finish
    fi
  done
done

cd "${CWD}"
exec /bin/s6-svscan /etc/s6
