
GULP_TASK=${GULP_TASK:-"default"}
NODE_CLI_EXEC=${NODE_CLI_EXEC:-"start"}

if [ -f "${1}/package.json" ]; then
  WORKSPACE=$(node -e  "process.stdout.write(require('${1}/package.json').name);")
  echo "Starting NPM start command for: ${WORKSPACE}"

  if [ $USE_WORKSPACES = 1 ]; then
    yarn workspace "${WORKSPACE}" run "${NODE_CLI_EXEC}"
  else
    cd $1
    yarn run "${NODE_CLI_EXEC}"
  fi

elif [ -f "./gulpfile.js" ] && [ -x "./node_modules/.bin/gulp" ]; then

cd $1
  PROJECT_NAME=$(basename "$1")
  echo "Running default Gulp task for: ${PROJECT_NAME}"
  gulp \"$GULP_TASK\"
fi
